var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
var Schema = mongoose.Schema;

var schema = new Schema({
    clientName: {
        type: String,
        required: false
    },
    clientPhone: {
        type: String,
        required: false
    },
    clientEmail: {
        type: String,
        required: false
    },
    clientMessage: {
        type: String,
        required: false
    },
    visiableStatus: {
        type: Boolean,
        required: true
    }
});


module.exports = mongoose.model('ClientsFeedback', schema);
