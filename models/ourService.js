var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
var Schema = mongoose.Schema;

var schema = new Schema({
    serviceImagePath: {
        type: String,
        required: true
    },
    serviceName: {
        type: String,
        required: true
    },
    serviceSlogan: {
        type: String,
        required: true
    },
    serviceShortInformation: {
        type: String,
        required: true
    },
    serviceFullInformation: {
        type: String,
        required: true
    },
    serviceStatusString: {
        type: String,
        required: false
    }
});


module.exports = mongoose.model('OurService', schema);
