var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
var Schema = mongoose.Schema;

var schema = new Schema({
    categoryName: {
        type: String,
        required: true
    },
    categoryCreatedDate: {
        type: Date,
        default: Date.now,
        required: false
    },
    categoryModifiedDate: {
        type: Date,
        default: Date.now,
        required: false
    },
    categoryCreatedAdmin: {
        type: String,
        required: false
    },
    categoryModifiedAdmin: {
        type: String,
        required: false
    },
    parentCategoryId: {
        type: String,
        required: false
    },
    childCategories: [{
        type: String,
        required: false
    }],
    categoryProducts: [{
        type: String,
        required: false
    }]
});


module.exports = mongoose.model('Categories', schema);
