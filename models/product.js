var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
var Schema = mongoose.Schema;

var schema = new Schema({
    imagePath: {
        type: String,
        required: true
    },
    productName: {
        type: String,
        required: true
    },
    productCreatedDate: {
        type: Date,
        default: Date.now,
        required: false
    },
    productModifiedDate: {
        type: Date,
        default: Date.now,
        required: false
    },
    productPrice: {
        type: Number,
        required: true
    },
    productCategory: {
        type: String,
        required: true
    },
    productShortInformation: {
        type: String,
        required: true
    },
    productFullInformation: {
        type: String,
        required: true
    },
    productViews: {
        type: Number,
        required: false
    },
    productStock: {
        type: Number,
        required: true
    },
    productStatusString: {
        type: String,
        required: false
    },
    productStatusBoolean: {
        type: Boolean,
        required: false
    },
    productReviews: [{
        userId: String,
        review: String,
        required: false
    }]
});


module.exports = mongoose.model('Product', schema);
