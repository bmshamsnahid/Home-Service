function displayUpdateServiceBlockModal(id, name, slogan, shortInfo, fullInfo, imagePath) {
    console.log("Modifying product id: " + id);
    console.log("Modifying product name: " + name);
    console.log("Modifying product slogan: " + slogan);
    console.log("Modifying product shortInfo: " + shortInfo);
    console.log("Modifying product fullInfo: " + fullInfo);
    console.log("Modifying product imagePath: " + imagePath);

    document.getElementById("formServiceId").setAttribute("value", id);
    document.getElementById("formServiceName").setAttribute("value", name);
    document.getElementById("formServiceSlogan").setAttribute("value", slogan);
    document.getElementById("formServiceShortInformation").setAttribute("value", shortInfo);
    document.getElementById("formServiceFullInformation").setAttribute("value", fullInfo);
    document.getElementById("formServiceImagePath").setAttribute("value", imagePath);

    document.getElementById('serviceModificationModalBlock').style.display='block';
}

function vanishUpdateServiceBlockModal() {
    document.getElementById('serviceModificationModalBlock').style.display='none';
}

function submitUpdateServiceModalForm() {
    document.getElementById("serviceUpdateForm").submit();
}