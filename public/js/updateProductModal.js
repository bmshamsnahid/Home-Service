function displayBlock(id, name, price, category, shortInfo, fullInfo, stock, views, imagePath) {
    console.log("Modifying product id: " + id);
    console.log("Modifying product name: " + name);
    console.log("Modifying product price: " + price);
    console.log("Modifying product category: " + category);
    console.log("Modifying product shortInfo: " + shortInfo);
    console.log("Modifying product fullInfo: " + fullInfo);
    console.log("Modifying product stock: " + stock);
    console.log("Modifying product views: " + views);
    console.log("Modifying product imagePath: " + imagePath);

    document.getElementById("formProductId").setAttribute("value", id);
    document.getElementById("formProductName").setAttribute("value", name);
    document.getElementById("formProductPrice").setAttribute("value", price);
    document.getElementById("formProductCategory").setAttribute("value", category);
    document.getElementById("formProductShortInformation").setAttribute("value", shortInfo);
    document.getElementById("formProductFullInformation").setAttribute("value", fullInfo);
    document.getElementById("formProductStock").setAttribute("value", stock);
    document.getElementById("formProductViews").setAttribute("value", views);
    document.getElementById("formProductImagePath").setAttribute("value", imagePath);

    document.getElementById('modificationForm').style.display='block';
}

function vanishBlock() {
    document.getElementById('modificationForm').style.display='none';
}

function submitModifyModalForm() {
    var categoryOption = document.getElementById("optionsCategoryListUpdateProduct");
    var categoryId = categoryOption.options[categoryOption.selectedIndex].value;
    console.log('Category Id : ' + categoryId);
    document.getElementById("formProductCategory").setAttribute('value', categoryId);
    document.getElementById("productUpdateForm").submit();
}