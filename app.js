var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var expressHbs = require('express-handlebars');     //better than the default handlebars
var mongoose = require('mongoose');     //manipulate the mongodb datatbase
var session = require('express-session');   //general sesssion maintance, also for csrf secuirrity
var passport = require('passport');     //for better authentication
var flash = require('connect-flash');   //display error or warning
var validator = require('express-validator');   //checking input field by not empty, is mail etc etc
var MongoStore = require('connect-mongo')(session);     //save the session data in database instead of memory
var dbConfig = require('./config/database');

var Product = require('./models/product');     //Product Scema is reqire to manip[ulate with product

var homeRoute = require('./routes/Shop/home');  //this is home routing
var bannerRoute = require('./routes/Shop/banner');   //this is banner route
var aboutRoute = require('./routes/Shop/about');   //this is about route
var addressRoute = require('./routes/Shop/address'); //this is address route
var serviceRoute = require('./routes/Shop/service'); //this is service route
var testimonialRoute = require('./routes/Shop/testimonial'); //this is testimonial route
var categoryExposed = require('./routes/Shop/categoryExposed'); //this is portfolio route
var categoriesDisplay = require('./routes/Shop/categoriesDisplay'); //categories summery display
var eventsRoute = require('./routes/Shop/events'); //this is events route
var workRoute = require('./routes/Shop/work'); //this is work route
var contactRoute = require('./routes/Shop/contact'); //this is contact route
var productExposedRoute = require('./routes/Shop/productExposed');  //display product information
//profile
var myCartRoute = require('./routes/Profile/myCart');
//administrator routes
var adminHomeRoute = require('./routes/Administrator/Home/adminHome');
//products route
var uploadProductRoute = require('./routes/Administrator/Products/uplaodProduct');
var productsListRoute = require('./routes/Administrator/Products/productsList');
var deleteProductRoute = require('./routes/Administrator/Products/deleteProduct');
var modifyProductRoute = require('./routes/Administrator/Products/modifyProduct');
//services route
var adminAddServiceRoute = require('./routes/Administrator/Service/adminAddService');
var adminServiceListRoute = require('./routes/Administrator/Service/adminServiceList');
var adminDeleteServiceRoute = require('./routes/Administrator/Service/adminDeleteService');
var adminUpdateServiceRoute = require('./routes/Administrator/Service/adminUpdateService');
//category route
var adminCategoryListRoute = require('./routes/Administrator/Category/adminCategoryList');
var adminAddCategoryRoute = require('./routes/Administrator/Category/adminAddCategory');
var adminUpdateCategoryRoute = require('./routes/Administrator/Category/adminUpdateCategory');
var adminDeleteCategoryRoute = require('./routes/Administrator/Category/adminDeleteCategory');
//user feedback
var adminFeedbackRoute = require('./routes/Administrator/Feedback/clientsFeedback');
//home banner route
var adminBannerRoute = require('./routes/Administrator/Banners/adminBanner');

//profile related route
var joinUsRoute = require('./routes/Authentication/joinUs');

var app = express();    //get the ecpress application

mongoose.connect(dbConfig.database, (err) => {
    if(err) {
        console.log('Error in connect database: ' + err);
    } else {
        console.log('Database connect successfully: ' + dbConfig.database);
    }
});

require('./config/passport');   //configaration fo sign in and up procedure

// view engine setup
//set the handlebars as the default templateing engine
app.engine('.hbs', expressHbs({
    defaultLayout: 'layout',    //set the ./views/layouts/layout.hbs as the default layout
    extname: '.hbs' //extension of the layout file
}));
app.set('view engine', '.hbs');     //setting the view engine finished

// MIDDLEWARE INITILIZATION
// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(validator());
app.use(cookieParser());
//set the express session
app.use(session({
    secret: 'mysupersecret',
    resave: false,
    saveUninitialized: false,
    store: new MongoStore({
        mongooseConnection: mongoose.connection
    }),
    cookie: {
        maxAge: 180 * 60 * 1000
    }
}));
app.use(flash());   //initialize the flash to display and grab error in authentiation
app.use(passport.initialize());     //initialize the passport to authenticaion help
app.use(passport.session());    //get the session in passport
app.use(express.static(path.join(__dirname, 'public')));    //set the static resource folder as ./public

app.use(function (req, res, next) {
    res.locals.login = req.isAuthenticated();  //each layout we need to check the if it's a user or guest
    res.locals.session = req.session;   //session is required to keep track user history: login, cart

    next();
});

//shop
app.use('/', homeRoute);    //home routing start with the home
app.use('/banner', bannerRoute);    //banner route start with /banner
app.use('/about', aboutRoute);  //about route start woth /about
app.use('/address', addressRoute);
app.use('/service', serviceRoute);
app.use('/testimonial', testimonialRoute);
app.use('/categoryExposed', categoryExposed);
app.use('/categoriesDisplay', categoriesDisplay);
app.use('/events', eventsRoute);
app.use('/work', workRoute);
app.use('/contact', contactRoute);
app.use('/productExposed', productExposedRoute);
//profile
app.use('/myCart', myCartRoute);
//administrator
app.use('/administrator', adminHomeRoute);
//products
app.use('/uploadProduct', uploadProductRoute);
app.use('/productsList', productsListRoute);
app.use('/deleteProduct', deleteProductRoute);
app.use('/modifyProduct', modifyProductRoute);
//service
app.use('/adminAddService', adminAddServiceRoute);
app.use('/adminServiceList', adminServiceListRoute);
app.use('/adminDeleteService', adminDeleteServiceRoute);
app.use('/adminUpdateService', adminUpdateServiceRoute);
//category
app.use('/adminCategoryList', adminCategoryListRoute);
app.use('/adminAddCategory', adminAddCategoryRoute);
app.use('/adminUpdateCategory', adminUpdateCategoryRoute);
app.use('/adminDeleteCategory', adminDeleteCategoryRoute);
//feedback
app.use('/adminFeedback', adminFeedbackRoute);
//banner
app.use('/adminBanner', adminBannerRoute);

app.use('/join', joinUsRoute);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;