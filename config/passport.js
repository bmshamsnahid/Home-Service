var passport = require('passport');
var User = require('../models/user');
var LoginApiConfig = require('./loginApiConfig');

//to run on local server it has to false
var isProduction = true;

//setting the necessary variables for the facebook authentication
var FacebookStrategy = require('passport-facebook').Strategy;
var facebookClientId = LoginApiConfig.facebookClientId;
var facebookClientSecret = LoginApiConfig.facebookClientSecret;
var facebookCallbackUrlDev = LoginApiConfig.facebookCallbackUrlDev;
var facebookCallbackUrlProduction = LoginApiConfig.facebookCallbackUrlProduction;
var facebookCallbackURL = isProduction ? facebookCallbackUrlProduction : facebookCallbackUrlDev;


var GoogleStrategy = require('passport-google-oauth20').Strategy;
var googleClientId = LoginApiConfig.googleClientId;
var googleClientSecret = LoginApiConfig.googleClientSecret;
var googleCallbackUrlDev = LoginApiConfig.googleCallbackUrlDev;
var googleCallbackUrlProduction = LoginApiConfig.googleCallbackUrlProduction;
var googleCallBackURL = isProduction ? googleCallbackUrlProduction : googleCallbackUrlDev;

passport.serializeUser(function(user, done) {
	  done(null, user);
});

passport.deserializeUser(function(user, done) {
	  done(null, user);
});

passport.use(new FacebookStrategy({
	clientID: facebookClientId,
    clientSecret: facebookClientSecret,
    callbackURL: facebookCallbackURL
  },
  function(accessToken, refreshToken, profile, cb) {
	  User.findOne({'facebookId': profile.id}, function(err, usr) {
          if(err) {   //error on e=fetching database
              console.log('Error in fetching database: ' + err);
          } else {    //no error, got the logged in user id
              console.log('No Error, Got data: ' + usr);
              //req.session.userId = usr._id;   //save the user id to the session
              if(!usr) {
            	  var newUser = new User();
                  newUser.facebookId = profile.id;
                  newUser.name = profile.displayName;
                  newUser.save(function (err, result) {
                      if(err) {
                          return cb(err);
                      }
                      return cb(null, newUser);
                  });
              } else {
            	  return cb(null, usr);
              }
              
          }
	  });
  }
));


passport.use(new GoogleStrategy({
    clientID: googleClientId,
    clientSecret: googleClientSecret,
    callbackURL: googleCallBackURL
  },
  function(accessToken, refreshToken, profile, cb) {
	  User.findOne({'googleId': profile.id}, function(err, usr) {
          if(err) {   //error on e=fetching database
              console.log('Error in fetching database: ' + err);
          } else {    //no error, got the logged in user id
              console.log('No Error, Got data: ' + usr);
              if(!usr) {
            	  var newUser = new User();
                  newUser.googleId = profile.id;
                  newUser.name = profile.displayName;
                  newUser.save(function (err, result) {
                      if(err) {
                          return cb(err);
                      }
                      return cb(null, newUser);
                      
                  });
              } else {
            	  return cb(null, usr);
              }

          }
	  });
  }
));