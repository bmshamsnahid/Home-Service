var express = require('express');
var router = express.Router();
var CategoryDB = require('../../../models/categories');

router.post('/', (req, res, next) => {
    console.log('Update category arena: ' + JSON.stringify(req.body));
    var categoryId = req.body.categoryId;
    var categoryName = req.body.categoryName;
    var parentCategoryId = req.body.parentCategoryId;
    var exParentCategoryId;

    console.log('category Id: ' + categoryId);

    CategoryDB.findById(categoryId, (err, mcCategory) => {
        if(err) {
            console.log('Error in fetching the category: ' + err);
        } else {
            console.log('Got the category: ' + JSON.stringify(mcCategory));

            exParentCategoryId = mcCategory.parentCategoryId;

            mcCategory.parentCategoryId = parentCategoryId;
            mcCategory.categoryName = categoryName;

            mcCategory.save((err, updatedCategory) => {
                if(err) {
                    console.log('Error in updating the category: ' + err);
                } else {
                    console.log('Category updated successfully: ' + updatedCategory);
                    if(exParentCategoryId != -1) {
                        CategoryDB.findById(exParentCategoryId, (err, exParentCategory) => {
                            if(err) {
                                console.log('error in fetching the exParent category');
                            } else {
                                console.log('successfully retrieved the exParentCategory');
                                var childCategoriesId = [];
                                childCategoriesId = exParentCategory.childCategories;
                                var currentCategoryIndex = childCategoriesId.indexOf(categoryId);
                                childCategoriesId.splice(currentCategoryIndex, 1);

                                exParentCategory.save((err, savedExParentCategory) => {
                                    if(err) {
                                        console.log('error in updating the exParentCategory');
                                    } else {
                                        console.log('successfully updated the exParentCategory: ' + savedExParentCategory);
                                    }
                                });
                            }
                        });
                    } else {

                    }
                    if(parentCategoryId != -1) {
                        CategoryDB.findById(parentCategoryId, (err, newParentCategory) => {
                            if(err) {
                                console.log('err in getting the new paren category: ' + err);
                            } else {
                                console.log('successfully retrieved the new parent category: ' + newParentCategory);
                                var childCategories = [];
                                childCategories = newParentCategory.childCategories;
                                childCategories.push(categoryId);

                                newParentCategory.childCategories = childCategories;

                                newParentCategory.save((err, savedNewParentCategory) => {
                                    if(err) {
                                        console.log('error in saving the new parent category: ' + err);
                                    } else {
                                        console.log('successfully updated the new parent category: ' + savedNewParentCategory);
                                    }
                                    //res.redirect('/adminCategoryList');
                                });
                            }
                        });
                    } else {

                    }
                    res.redirect('/adminCategoryList');
                }
            });
        }
    });

});

module.exports = router;