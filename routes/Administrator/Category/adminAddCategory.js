var express = require('express');
var router = express.Router();
var CategoryDB = require('../../../models/categories');

router.get('/', (req, res, next) => {
    CategoryDB.find((err, categories) => {
        if(err) {
            console.log('Error in fetching the category database: ' + err);
            res.render('Administrator/Category/adminAddCategory', {
                title: 'Add Category',
                categories: null
            });
        } else {
            console.log('Successfully retrieved all the categories');
            res.render('Administrator/Category/adminAddCategory', {
                title: 'Add Category',
                categories: categories
            });
        }
    });

});

router.post('/', (req, res, next) => {
    console.log('add category post request: ' + JSON.stringify(req.body));

    var parentCategoryId = req.body.parentCategoryId;
    var parentCategoryName = req.body.parentCategoryName;
    var categoryName = req.body.categoryName;

    console.log('p Id: ' + parentCategoryId);
    console.log('p Name: ' + parentCategoryName);
    console.log('cat Name: ' + categoryName);

    var newCategory = new CategoryDB({
        categoryName: categoryName,
        parentCategoryId: parentCategoryId
    });

    newCategory.save((err, savedCategory) => {
        if(err) {
            console.log('Error in saving the new Category');
            res.redirect('/adminAddCategory');
        } else {
            console.log('new category saved');
            if(parentCategoryId != -1) {
                CategoryDB.findById(parentCategoryId, (err, parentCategory) => {
                    if(err) {
                        console.log('Error in finding the parent category: ' + err);
                        res.redirect('/adminAddCategory');
                    } else {
                        console.log('The saved categories is: ' + savedCategory);
                        console.log('Saved category id: ' + savedCategory._id);

                        var childCategories = [];
                        childCategories = parentCategory.childCategories;
                        childCategories.push(savedCategory._id);

                        parentCategory.childCategories = childCategories;

                        parentCategory.save((err) => {
                            if(err) {
                                console.log('Error in updating the parent category');
                                res.redirect('/adminAddCategory');
                            } else {
                                console.log('Successfully updated the parent category');
                                res.redirect('/adminCategoryList');
                            }
                        });
                    }
                });
            } else {
                res.redirect('/adminCategoryList');
            }
        }
    });

});

module.exports = router;