var express = require('express');
var router = express.Router();
var CategoryDB = require('../../../models/categories');

router.get('/', (req, res, next) => {
    CategoryDB.find((err, categories) => {
        if(err) {
            console.log('Error in fetching all the categories');
            res.render('Administrator/Category/adminCategoryList.hbs', {
                title: 'Category List'
            });
        } else {
            console.log('got all the categories');
            res.render('Administrator/Category/adminCategoryList.hbs', {
                title: 'Category List',
                categories: categories
            });
        }
    });
});

module.exports = router;