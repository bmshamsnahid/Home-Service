var express = require('express');
var router = express.Router();
var CategoryDB = require('../../../models/categories');

router.get('/:categoryId', (req, res, next) => {

    var categoryId = req.params.categoryId;

    console.log('Category Id: ' + categoryId);

    CategoryDB.findById(categoryId, (err, mcCategory) => {

        if(err) {
            console.log('Error in getting the mc category: ' + err);
        } else {
            //remove from the chidl category list of its parent category
            if(mcCategory.parentCategoryId != -1) {
                CategoryDB.findById(mcCategory.parentCategoryId, (err, parentCategory) => {
                    if(err) {
                        console.log('Error in finding the parent category');
                    } else {
                        var childCategories = [];
                        childCategories = parentCategory.childCategories;
                        var mcCategoryIndex = childCategories.indexOf(categoryId);
                        if(mcCategoryIndex > -1) {
                            childCategories.splice(mcCategoryIndex, 1);
                        }
                        parentCategory.childCategories = childCategories;
                        parentCategory.save((err) => {
                            if(err) {
                                console.log('Error in updating the parent category');
                            } else {
                                console.log('Parent category updated succesfully');
                            }
                            mcCategory.remove((err, deletedCategory) => {
                                if(err) {
                                    console.log('Error in deleting the category: ' + err);
                                } else {
                                    console.log('Category removed successfully');
                                    for(var index=0; index<mcCategory.childCategories.length; index++) {
                                        console.log('Child Category Id: ' + mcCategory.childCategories[index]);
                                        CategoryDB.findById(mcCategory.childCategories[index], (err, childCategory) => {
                                            if(err) {
                                                console.log('Error in fetching the child category info: ' + err);
                                            } else {
                                                console.log('Got the child category');
                                                childCategory.parentCategoryId = -1;
                                                childCategory.save((err) => {
                                                    if(err) {
                                                        console.log('Error in updating the child category: ' + err);
                                                    } else {
                                                        console.log('Child category updated successfully');
                                                    }
                                                });
                                            }
                                        });
                                    }
                                }
                            });
                        });
                    }
                });
            } else {
                mcCategory.remove((err, deletedCategory) => {
                    if(err) {
                        console.log('Error in deleting the category: ' + err);
                    } else {
                        console.log('Category removed successfully');
                        for(var index=0; index<mcCategory.childCategories.length; index++) {
                            console.log('Child Category Id: ' + mcCategory.childCategories[index]);
                            CategoryDB.findById(mcCategory.childCategories[index], (err, childCategory) => {
                                if(err) {
                                    console.log('Error in fetching the child category info: ' + err);
                                } else {
                                    console.log('Got the child category');
                                    childCategory.parentCategoryId = -1;
                                    childCategory.save((err) => {
                                        if(err) {
                                            console.log('Error in updating the child category: ' + err);
                                        } else {
                                            console.log('Child category updated successfully');
                                        }
                                    });
                                }
                            });
                        }
                    }
                });
            }

        }
    });

    res.redirect('/adminCategoryList');
});

module.exports = router;