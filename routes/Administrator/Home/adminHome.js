var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
    res.render('Administrator/adminHome', { title: 'Admin Home' });
});

module.exports = router;