var express = require('express');
var router = express.Router();
var OurService = require('../../../models/ourService');

var multer = require('multer');
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/images/uploads/services')   //declare the upload image directory
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + '_Service' + file.originalname)      //set image file name: current time and data plus main image name in local directory
    }
})
var upload = multer({ storage: storage });   //now upload will handle desired multer configation


router.get('/', function(req, res, next) {
    console.log('Working on service manipulation');
    res.render('Administrator/adminAddService', { title: 'Create Service' });
});

router.post('/', upload.single('image'), function(req, res, next) {

    //here we get the necessary information about the products
    var serviceName = req.body.serviceName;
    var serviceSlogan = req.body.serviceSlogan;
    var serviceShortInformation = req.body.serviceShortInformation;
    var serviceFullInformation = req.body.serviceFullInformation;

    console.log('Name: ' + serviceName + '\nslogan: ' + serviceSlogan + '\nshort info: ' + serviceShortInformation + '\nfull Info: ' + serviceFullInformation);

    //create the product model, according to the schema
    var ourService = new OurService({
        serviceImagePath: '/images/uploads/services/' + req.file.filename,
        serviceName: serviceName,
        serviceSlogan: serviceSlogan,
        serviceShortInformation: serviceShortInformation,
        serviceFullInformation: serviceFullInformation
    });

    ourService.save(function(err, result) {
        if(err) {
            console.log('>>>>>>>>>>>>>>>>Error occoured on adding the service: ' + err);
            res.render('Admin/adminAddService');
        } else {
            console.log('>>>>>>>>>>>>>>>> Successfully updated the product');
            return res.redirect('/administrator');
        }
    });

});

module.exports = router;