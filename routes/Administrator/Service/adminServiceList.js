var express = require('express');
var router = express.Router();
var OurService = require('../../../models/ourService');

router.get('/', function(req, res, next) {
    var ourService = OurService.find(function (err, docs) {
        if(err) {   //error in fatching database
            console.log('Error Happened'  + err);
            return res.redirect('/administrator');
        } else {    //got the products
            res.render('Administrator/adminServiceList', {
                products: docs  //sending all the products
            });
        }
    });
});

module.exports = router;