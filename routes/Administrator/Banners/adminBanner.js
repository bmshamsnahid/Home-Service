var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
    res.render('shop/Home/about', { title: 'About' });
});

router.get('/test', function(req, res, next) {
    res.render('shop/Home/about', { title: 'About' });
});

module.exports = router;