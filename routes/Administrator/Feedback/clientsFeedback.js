var express = require('express');
var router = express.Router();
var ClientsFeedbackDB = require('../../../models/clientsFeedback');

router.get('/displayFeedbacklist', (req, res, next) => {
    res.send('this is display feedback');
});

router.post('/addFeedback', (req, res, next) => {
    console.log('received: ' + JSON.toString(req.body));

    var clientName = req.body.clientName;
    var clientPhone = req.body.clientPhone;
    var clientEmail = req.body.clientEmail;
    var clientMessage = req.body.clientMessage;

    console.log('name: ' + clientName + ' phone: ' + clientPhone + ' email: ' + clientEmail + ' message: ' + clientMessage);

    var newFeedback = new ClientsFeedbackDB({
        clientName: clientName,
        clientPhone: clientPhone,
        clientEmail: clientEmail,
        clientMessage: clientMessage,
        visiableStatus: false
    });

    newFeedback.save((err, savedFeedback) => {
        if(err) {
            console.log('error in saving the new clients feedack');
        } else {
            console.log('successfully updated the feedback: ' + savedFeedback);
            res.redirect('/');
        }
    });
});

module.exports = router;