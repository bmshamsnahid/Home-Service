var express = require('express');
var router = express.Router();
var Product = require('../../../models/product');
var fs = require('fs');

router.get('/:productId', function(req, res, next) {
    var productId = req.params.productId;
    console.log('Deleting the product: ' + productId);
    var imagePath = null;


    Product.findById(productId, (err, product) => {
        if(err) {
            console.log('Error in finding the product');
            res.redirect('/productsList');
        } else {
            console.log('Name: ' + product.productName);
            console.log('Image path : ./public' +  product.imagePath);
            console.log('Id: ' + product._id);
            product.remove((err, success) => {
                if(err) {
                    console.log('Error in removinf the product: ' + err);
                } else {
                    console.log('Product Removed successfully.');
                    fs.unlink('./public' + product.imagePath, (err) => {
                        if (err) {
                            console.log('Error in removinf image file: ' + product.imagePath + " error: " + err);
                        } else {
                            console.log('Removed image file: ' + product.imagePath);
                        }
                        res.redirect('/productsList');
                    });
                }
            });
        }
    });
});

module.exports = router;