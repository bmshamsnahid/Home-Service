var express = require('express');
var router = express.Router();
var Product = require('../../../models/product');
var CategoryDB = require('../../../models/categories');

var multer = require('multer');
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/images/uploads')   //declare the upload image directory
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + '-' + file.originalname)      //set image file name: current time and data plus main image name in local directory
    }
})
var upload = multer({ storage: storage })   //now upload will handle desired multer configation


router.get('/', function(req, res, next) {
    console.log('Working on product manipulation');

    CategoryDB.find((err, categories) => {
        if(err) {
            console.log('error in finding the categories' + err);
        } else {
            res.render('Administrator/Products/uploadProduct', {
                title: 'Upload Product',
                categories: categories
            });
        }
    });
});

router.post('/', upload.single('image'), function(req, res, next) {

    //here we get the necessary information about the products
    var productName = req.body.productName;
    var productPrice = req.body.productPrice;
    var productCategoryId = req.body.productCategory;
    var productShortInformation = req.body.productShortInformation;
    var productFullInformation = req.body.productFullInformation;
    var productViews = 0;
    var productStock = req.body.productStock;

    console.log('Name: ' + productName + '\nprice: ' + productPrice + '\ncategory: ' + productCategoryId + '\nshort info: ' + productShortInformation + '\nfull Info: ' + productFullInformation + '\nstock: ' + productStock + '\nviews: ' + productViews);

    //create the product model, according to the schema
    var product = new Product({
        imagePath: '/images/uploads/' + req.file.filename,
        productName: productName,
        productPrice: productPrice,
        productCategory: productCategoryId,
        productShortInformation: productShortInformation,
        productFullInformation: productFullInformation,
        productViews: productViews,
        productStock: productStock
    });

    product.save(function(err, savedProduct) {
        if(err) {
            console.log('>>>>>>>>>>>>>>>>Error occoured on adding the product: ' + err);
            res.render('Admin/addProduct');
        } else {
            console.log('>>>>>>>>>>>>>>>> Successfully updated the product');
            CategoryDB.findById(productCategoryId, (err, retrievedCategory) => {
                if(err) {
                    console.log('error in finding the category: ' + err);
                } else {
                    console.log('Successfully retrieved the category: ' + retrievedCategory);
                    var categoryProducts = [];
                    categoryProducts = retrievedCategory.categoryProducts;
                    categoryProducts.push(savedProduct._id);
                    retrievedCategory.categoryProducts = categoryProducts;
                    retrievedCategory.save((err, updatedCategory) => {
                        if(err) {
                            console.log('err in updating the products id info to the category: ' + err);
                        } else {
                            console.log('successfully updated the category: ' + updatedCategory);
                            return res.redirect('/administrator');
                        }
                    });
                }
            });
        }
    });

});

module.exports = router;