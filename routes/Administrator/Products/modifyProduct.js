var express = require('express');
var router = express.Router();
var Product = require('../../../models/product');
var fs = require('fs');

router.post('/', (req, res, next) => {
    console.log('This is product modify arera: ' + JSON.stringify(req.body));

    var productId = req.body.productId;
    var productName = req.body.productName;
    var productPrice = req.body.productPrice;
    var productCategory = req.body.productCategory;
    var productShortInformation = req.body.productShortInformation;
    var productFullInformation = req.body.productFullInformation;
    var productViews = req.body.productViews;
    var productStock = req.body.productStock;
    var productImagePath = req.body.productImagePath;

    console.log('id: ' + productId + '\nName: ' + productName + '\nprice: ' + productPrice + '\ncategory: ' + productCategory + '\nshort info: ' + productShortInformation + '\nfull Info: ' + productFullInformation + '\nstock: ' + productStock + '\nviews: ' + productViews + '\nimagePath: ' + productImagePath);

    Product.findById(productId, (err, product) => {
        if(err) {
            console.log('Error in finding the desired product: ' + err);
            res.redirect('/productsList');
        } else {
            product.productName = productName;
            product.productPrice = productPrice;
            product.productCategory = productCategory;
            product.productShortInformation = productShortInformation;
            product.productFullInformation = productFullInformation;
            product.productViews = productViews;
            product.productStock = productStock;
            product.productImagePath = productImagePath;

            product.save((err) => {
                if(err) {
                    console.log('Error in updating product: ' + err);
                } else {
                    console.log('Product updated successfullt');
                }
                res.redirect('/productsList');
            });
        }
    });
});

module.exports = router;