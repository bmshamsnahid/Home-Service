var express = require('express');
var router = express.Router();
var Product = require('../../../models/product');
var CategoryDB = require('../../../models/categories');

router.get('/', function(req, res, next) {
    var products = Product.find(function (err, docs) {
        if(err) {   //error in fatching database
            console.log('Error Happened'  + err);
            return res.redirect('/');
        } else {    //got the products
            CategoryDB.find((err, categories) => {
                if(err) {
                    console.log('err in finding the categories: ' + err);
                } else {
                    res.render('Administrator/productsList', {
                        products: docs,
                        categories: categories
                    });
                }
            });
        }
    });
    //res.render('', { title: 'Product List' });
});

module.exports = router;