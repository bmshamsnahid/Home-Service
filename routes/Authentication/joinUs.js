var express = require('express');
var router = express.Router();
var passport = require('passport');
var User = require('../../models/user');

router.get('/', (req, res, next) => {
    res.render('shop/Home/joinUs');
});

router.get('/logout', isLoggedIn, function (req, res, next) {
    req.session.isUser = false;
    req.logout();
    res.redirect('/');
});

router.get('/', notLoggedIn, function (req, res, next) {
    next();
});

router.get('/auth/facebook', passport.authenticate('facebook'));
router.get('/login/facebook/return', passport.authenticate('facebook',
    { failureRedirect: '/join' }),
    function(req, res) {
        // Successful authentication, redirect home.
        req.session.userId = req.user._id;
        req.session.username = req.user.name;
        res.redirect('/');
});

router.get('/auth/google', passport.authenticate('google', { scope: ['profile'] }));
router.get('/login/google/return', passport.authenticate('google',
    { failureRedirect: '/join' }),
    function(req, res) {
        // Successful authentication, redirect home.
        req.session.userId = req.user._id;
        req.session.username = req.user.name;
        res.redirect('/');
});

module.exports = router;

function isLoggedIn(req, res, next) {
    if(req.isAuthenticated()) {
        return next();
    }
    res.redirect('/');
}

function notLoggedIn(req, res, next) {
    if(!req.isAuthenticated()) {
        return next();
    }
    res.redirect('/');
}