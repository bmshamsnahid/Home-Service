var express = require('express');
var router = express.Router();
var UserDB = require('../../models/user');
var ProductDB = require('../../models/product');

router.get('/showCart', (req, res, next) => {
    var userId = (req.user._id).toString();

    UserDB.findById(userId, (err, retrievedUser) => {
        if(err) {
            console.log('error in retrieving the user info: ' + err);
        } else {
            var userCarts = retrievedUser.cart;
            for(var index=0; index<userCarts.length; index++) {
                console.log('user carts: ' + userCarts[index]._id);
            }
            var cartsProducts = [];
            ProductDB.find((err, allProducts) => {
                if(err) {
                    console.log('error in finding the products: ' + err);
                } else {
                    console.log('got all the products');
                    for(var indexP=0; indexP<allProducts.length; indexP++) {
                        for(var index=0; index<userCarts.length; index++) {
                            console.log('user carts: ' + userCarts[index]._id);
                            if((allProducts[indexP]._id).toString() == (userCarts[index]._id).toString()) {
                                cartsProducts.push(allProducts[indexP]);
                            }
                        }
                        if(indexP == allProducts.length-1) {
                            res.render('shop/Cart/displayCart', {
                                products: cartsProducts
                            });
                        }
                    }
                }
            });
        }
    });
});

router.get('/addToCart/:productId', (req, res, next) => {
    console.log('My add to cart arena');

    var productId = req.params.productId;
    var userId = (req.user._id).toString();

    console.log('productId: ' + productId + ' userId: ' + userId);

    UserDB.findById(userId, (err, currentUser) => {
        if(err) {
            console.log('err in finding the current user: ' + err);
        } else {
            currentUser.cart.push(productId);
            currentUser.save((err, savedUser) => {
                if(err) {
                    console.log('err in updating the cart: ' + err);
                } else {
                    console.log('cart updated successfully: ' + savedUser);
                    res.redirect('/productExposed/' + productId);
                }
            });
        }
    });
});

router.get('/removeFromCart/:productId', (req, res, next) => {
    console.log('My remove from cart arena');

    var productId = req.params.productId;
    var userId = (req.user._id).toString();

    console.log('productId: ' + productId + ' userId: ' + userId);

    UserDB.findById(userId, (err, currentUser) => {
        if(err) {
            console.log('err in finding the current user: ' + err);
        } else {
            var mcProductIndex = currentUser.cart.indexOf(productId);
            console.log('mc product index: ' + mcProductIndex);

            var isRemoved = false;

            for(var index=0; index<currentUser.cart.length; index++) {
                if((currentUser.cart[index]._id).toString() == productId.toString()) {
                    currentUser.cart.splice(index, 1);
                    isRemoved = true;
                }
            }
            if(isRemoved == true) {
                currentUser.save((err, savedUser) => {
                    if(err) {
                        console.log('err in updating the cart: ' + err);
                    } else {
                        console.log('cart updated successfully: ' + savedUser);
                        res.redirect('/myCart/showCart');
                    }
                });
            }
        }
    });
});

module.exports = router;