var express = require('express');
var router = express.Router();
var Product = require('../../models/product');
var CategoryDB = require('../../models/categories');

/*
router.get('/', function(req, res, next) {
    Product.find((err, products) => {
        if(err) {
            console.log('Error in fetching the products: ' + err);
            res.redirect('/');
        } else {
            CategoryDB.find((err, categories) => {
                if(err) {
                    console.log('error in finding the categories: ' + err);
                } else {
                    res.render('shop/Home/categoriesDisplay', {
                        products : products,
                        categories: categories
                    });
                }
            });
        }
    });
});
*/

//CATEGORY SUMMARY ROUTING//
//Here we finnd all the product and filter them
//first get all the product category
//then according to the category, product will be inserted
//result will be, for each category, all products of these category will be easily found
router.get('/', function(req, res, next) {

    //try to get all products from database
    CategoryDB.find((err, allCategories) => {
        if(err) {
            console.log('error in finding all the categories: ' + err);
        } else {

            console.log('Areana of category mapping');
            var categoryMap = new Map();
            for(var index=0; index<allCategories.length; index++) {
                console.log('cat Id: ' + allCategories[index]._id + ' name: ' + allCategories[index].categoryName);
                categoryMap.set(allCategories[index]._id, allCategories[index].categoryName);
            }
            for(var index=0; index<allCategories.length; index++) {
                console.log(categoryMap.get(allCategories[index]._id));
            }

            var products = Product.find(function (err, docs) {
                if(err) {   //error in fatching database
                    console.log('Error Happened'  + err);
                    return res.redirect('/');
                } else {    //got the products
                    var mapArr = [];    //here we put mapObj, where we kepp category name and all products of that category
                    var category = [];  //all category of the products will be kept here

                    var testEnd = 0;

                    for(var index=0; index<allCategories.length; index++) {
                        console.log('cat Id: ' + allCategories[index]._id + ' name: ' + allCategories[index].categoryName);
                        categoryMap.set((allCategories[index]._id).toString(), allCategories[index].categoryName);
                    }

                    //filtering products category
                    for(var product in docs) {  //traverse all the products
                        testEnd ++;
                        //console.log('Product Information: ' + docs[product]); //printing the products name
                        if(docs[product].productCategory != null) {    //checking if a products category is exist
                            if(category.includes(docs[product].productCategory) == false) {    //checking if the category is not in array
                                category.push(docs[product].productCategory);  //add the product category to the array
                                //for a new category, mapObj is created
                                //here we store the category name and array of that category's product
                                console.log('new category Id: ' + docs[product].productCategory);
                                console.log('new category name: ' + categoryMap.get((docs[product].productCategory).toString()));
                                var mapObj = {
                                    'catId': docs[product].productCategory,
                                    'catName': categoryMap.get(docs[product].productCategory),
                                    'pds': [docs[product]]
                                };
                                console.log('Map Object PDS is: ' + mapObj.pds);
                                mapArr.push(mapObj);    //insert the product category object
                            } else {
                                //since category is exist in map, so we find the category and insert the product
                                for(mp in mapArr) { //traverssing all the product categoy
                                    if(mapArr[mp].catId == docs[product].productCategory) {   //found the current product category object, now we insert the product
                                        mapArr[mp].pds.push(docs[product]); //pushing the products
                                    }
                                }
                            }

                        }
                        if(testEnd == docs.length) {
                            res.render('shop/Home/categoriesDisplay', {
                                categories: allCategories, //sending category
                                mapArr: mapArr  //sending filtered product
                            });
                        }
                    }
                }
            });
        }
    });
});

module.exports = router;