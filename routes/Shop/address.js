var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
    res.render('shop/Home/address', { title: 'Address' });
});

module.exports = router;