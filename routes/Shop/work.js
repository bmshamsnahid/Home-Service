var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
    res.render('shop/Home/work', { title: 'Work' });
});

module.exports = router;