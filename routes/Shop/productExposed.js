var express = require('express');
var router = express.Router();
var ProductDB = require('../../models/product');

router.get('/:productId', (req, res, next) => {
    var productId = req.params.productId;

    console.log('Logged in user info: ' + JSON.stringify(req.user));
    console.log('Logged in user id: ' + req.user._id);

    console.log('this is product exposed arena: ' + productId);

    ProductDB.findById(productId, (err, retrievedProduct) => {
        if(err) {
            console.log('err in find a specific product: ' + err);
        } else {
            res.render('shop/Home/productExposed', {
                title: retrievedProduct.productName,
                product: retrievedProduct
            });
        }
    });
});

router.post('/productReview/:productId', (req, res, next) => {
    var productId = req.params.productId;
    var userId = (req.user._id).toString();
    var newReview = (req.body.productReview).trim();

    console.log('Add product review arena: ');
    console.log('productId: ' + productId);
    console.log('user Id: ' + userId);
    console.log('newReview: ' + newReview);

    ProductDB.findById(productId, (err, retrievedProduct) => {
        if(err) {
            console.log('err in retrieving the product: ' + err);
        } else {
            retrievedProduct.productReviews.push({
                userId: userId,
                review: newReview
            });

            retrievedProduct.save((err, savedProduct) => {
                if(err) {
                    console.log('error in updating the product with review: ' + err);
                }  else {
                    console.log('product updated successfully with the new review: ' + savedProduct);
                    res.redirect('/productExposed/' + productId);
                }
            });
        }
    });

});

module.exports = router;