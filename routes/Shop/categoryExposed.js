var express = require('express');
var router = express.Router();
var Product = require('../../models/product');
var CategoryDB = require('../../models/categories');

router.get('/:categoryId', function(req, res, next) {
    var categoryId = req.params.categoryId;
    console.log('Category Id: ' + categoryId);

    CategoryDB.findById(categoryId, (err, retrievedCategory) => {
        if(err) {
            console.log('err in finding the category: ' + err);
            res.redirect('/categoriesDisplay');
        } else {
            Product.find((err, allProducts) => {
                if(err) {
                    console.log('Error in fetching the products: ' + err);
                    res.redirect('/categoriesDisplay');
                } else {
                    var categorizedProducts = [];

                    for(var index=0; index<allProducts.length; index++) {
                        console.log('index: ' + index + ' length: ' + allProducts.length);
                        if(allProducts[index].productCategory == categoryId) {
                            categorizedProducts.push(allProducts[index]);
                        }

                        if(index == allProducts.length - 1) {
                            res.render('shop/Home/categoryExposed', {
                                products : categorizedProducts,
                                categoryName: retrievedCategory.categoryName
                            });
                        }
                    }
                }
            });
        }
    });
});

module.exports = router;