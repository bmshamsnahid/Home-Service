var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
    res.render('shop/Home/testimonial', { title: 'Testimonial' });
});

module.exports = router;