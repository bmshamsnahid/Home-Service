var express = require('express');
var router = express.Router();
var OurService = require('../../models/ourService');

/* GET home page. */
router.get('/', function(req, res, next) {
    OurService.find((err, services) => {
        if(err) {
            console.log('Error in fetching services: ' + services);
             res.render('shop/Home/service', { title: 'Service' });
        } else {
            res.render('index', {
                title: 'Home',
                ourServices: services
            });
        }
    });
});

module.exports = router;
